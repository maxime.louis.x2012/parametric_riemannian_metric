import os

#geodesic_sampling_widths = [0.5, 0.3, 0.2, 0.1]
geodesic_sampling_widths = [1.]
#n_directions = [1, 3, 7, 10]
n_directions = range(200, 400, 20)

cpu_no = 0
cpu_per_task = 1

kill_sessions = []

for width in geodesic_sampling_widths:
    for n_dir in n_directions:
        session_name = 'metric_from_geodesics_{}_{}'.format(width, n_dir)
        scripts_args = '-width {} -n_dir {}'.format(width, n_dir)
        cpu = '{}-{}'.format(cpu_no, cpu_per_task + cpu_no)
        cpu_no += cpu_per_task
        print('tmux new -d -s "{}" \'taskset -c {} python metric_from_geodesics.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))

print('')
for elt in kill_sessions:
    print(elt)

