from torch.optim import Adam
from torch.utils.data import DataLoader
import torch
import numpy as np
import os
from torch.optim.lr_scheduler import StepLR
from utils import plot_progression


def estimate_parametric_model(model, dataset, n_epochs=10000,
                                learning_rate=5e-4, output_dir='output',
                                batch_size=32, save_every_n_iters=50,
                                print_every_n_iters=1,
                                call_back=None, lr_decay=0.95, l=0.1,
                                randomize_nb_observations=False, estimate_noise=True):

    print('In estimation.')
    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

    model.linear_initialization(dataset)

    print('l {}, lr {}, decay {}, batch_size {}'.format(l, learning_rate, lr_decay, batch_size))

    train_losses = []
    reconstruction_losses = []
    regularity_losses = []

    noise_variance = l

    switch_iter = 300

    optimizer = Adam(model.parameters(), lr=learning_rate)

    for epoch in range(n_epochs):
        # if epoch % switch_iter == 0:
        #     if (epoch/switch_iter) % 2 == 0:
        #         optimizer = Adam(model.parameters(mode='encoder'), lr=learning_rate)
        #         print('Training encoder')
        #     else:
        #         print('Training pop parameters')
        #         optimizer = Adam(model.parameters(mode='pop'), lr=learning_rate)

        train_loss = 0.
        reconstruction_loss = 0.
        regularity_loss = 0.

        no_in_batch = 0
        n_batch = 0
        n_el = 0

        times_batch = torch.zeros(0).type(dataset.type)
        sources_batch = torch.zeros(0).type(dataset.type)

        xis_batch = torch.zeros(0).type(dataset.type)
        sources_unique_batch = torch.zeros(0).type(dataset.type)
        taus_batch = torch.zeros(0).type(dataset.type)
        values_batch = torch.zeros(0).type(dataset.type)

        for i, data in enumerate(dataloader):

            times = data['times'].squeeze(0)
            ages = data['ages'].squeeze(0)
            values = data['values'].squeeze(0)

            if randomize_nb_observations and len(times) > 2:
                n_to_keep = np.random.randint(2, len(times))
                indices_to_keep = np.sort(np.random.choice(range(len(times)), n_to_keep, replace=False))
                times = times[indices_to_keep]
                ages = ages[indices_to_keep]
                values = values[indices_to_keep]

            indiv_var = model.encode(times, values)

            xi, tau, sources, geodesic_times = model.get_individual_info(ages, indiv_var)

            values_batch = torch.cat([values_batch, values], 0)
            times_batch = torch.cat([times_batch, geodesic_times], 0)
            for _ in times:
                sources_batch = torch.cat([sources_batch, sources.view(1, model.nb_sources)], 0)

            xis_batch = torch.cat([xis_batch, xi.view(1)], 0)
            sources_unique_batch = torch.cat([sources_unique_batch, sources.view(1, model.nb_sources)], 0)
            taus_batch = torch.cat([taus_batch, tau.view(1)], 0)

            no_in_batch += 1

            # If we have gathered all the data for one batch, we perform a step of gradient descent:
            if no_in_batch % batch_size == 0 or i == len(dataloader)-1:

                # Zero-ing the gradients
                optimizer.zero_grad()

                #MSE Loss
                reconstructed = model.decode(times_batch, sources_batch)
                reconstruction = torch.sum((reconstructed - values_batch)**2)

                regularity = noise_variance * model.compute_regularity(xis_batch, taus_batch, sources_unique_batch)

                # loss = regularity + reconstruction
                loss = reconstruction

                loss.backward(retain_graph=True)
                optimizer.step()
                model.project_metric_parameters()

                train_loss += loss.cpu().detach().numpy()
                reconstruction_loss += reconstruction.cpu().detach().numpy()
                regularity_loss += regularity.cpu().detach().numpy()

                if estimate_noise:
                    noise_variance = float(reconstruction.cpu().detach().numpy() / reconstructed.numel())

                xis_batch = torch.zeros(0).type(dataset.type)
                sources_unique_batch = torch.zeros(0).type(dataset.type)
                sources_batch = torch.zeros(0).type(dataset.type)
                times_batch = torch.zeros(0).type(dataset.type)
                values_batch = torch.zeros(0).type(dataset.type)

                no_in_batch = 0
                n_batch += 1
                n_el += reconstructed.numel()

        train_losses.append(train_loss/n_batch)
        reconstruction_losses.append(reconstruction_loss/n_el)
        regularity_losses.append(regularity_loss/n_batch)

        if epoch % print_every_n_iters == 0:

            lr = 0.
            for param_group in optimizer.param_groups:
                lr = [param_group['lr']][0]
                break

            print('{}/{}, train loss {}, reconstruction loss {}, regularity loss {}, lr {}, noise variance {}'.format(epoch, n_epochs, train_loss, reconstruction_loss/n_el, regularity_loss, lr, noise_variance))

            print('t0', model.t0.detach().numpy(), 'p0', model.p0.detach().numpy(), 'v0', model.v0.detach().numpy())

        # We save a plot of 4 individuals at the end of each epoch
        if epoch % save_every_n_iters == 0 or epoch == n_epochs-1:
            plot_progression([train_losses, regularity_losses, reconstruction_losses],
                             ['train', 'regularity', 'reconstruction'], output_dir, 'train_loss.pdf')

            model.plot_reconstructions(dataloader, output_dir)

            model.plot_average_trajectory(output_dir)
            model.save(output_dir)

            if call_back is not None:
                call_back(model)

    np.save(os.path.join(output_dir, 'train_losses.npy'), train_losses)
    np.save(os.path.join(output_dir, 'reconstruction_losses.npy'), reconstruction_losses)
    np.save(os.path.join(output_dir, 'regularity_losses.npy'), regularity_losses)



