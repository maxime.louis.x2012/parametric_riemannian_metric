import numpy as np
from torch.utils.data import Dataset
import torch


class LongitudinalDataset(Dataset):
    def __init__(self, ids, times,  ages_std=None, ages_mean=None, type=torch.DoubleTensor):

        assert len(ids) == len(times)

        self.type = type
        self.ids = ids

        self.ages = times  # the unrescaled ages, we always keep them.
        self.ages_std = ages_std
        self.ages_mean = ages_mean
        if self.ages_std is None:
            self.ages_std = np.std(self.ages)
        if self.ages_mean is None:
            self.ages_mean = np.mean(self.ages)

        self.times = (self.ages - self.ages_mean) / self.ages_std
        self.times_per_id = {}
        self.ages_per_id = {}
        self.values_per_id = {}
        self.rids = []
        self.min_time = float('inf')
        self.max_time = -float('inf')

        print('Working on {} subjects with a total of {} visits'.format(len(set(ids)), len(ids)))

        self.values_per_id = {}


    def load_value(self, i):
        raise NotImplementedError('Please implement this in the child class.')

    def update(self):
        for (i, idx, t, age) in zip(range(len(self.ids)), self.ids, self.times, self.ages):
            value = self.load_value(i)

            if idx not in self.times_per_id.keys():
                self.rids.append(idx)
                self.values_per_id[idx] = [value]
                self.times_per_id[idx] = [t]
                self.ages_per_id[idx] = [age]
            else:

                self.values_per_id[idx].append(value)
                self.times_per_id[idx].append(t)
                self.ages_per_id[idx].append(age)

        for rid in self.times_per_id.keys():
            # Sorting everything by time and removing single visit subjects
            self.times_per_id[rid] = np.array(self.times_per_id[rid])

            self.values_per_id[rid] = np.array(self.values_per_id[rid])

            self.ages_per_id[rid] = np.array(self.ages_per_id[rid])

            order = np.argsort(self.times_per_id[rid])

            self.times_per_id[rid] = self.times_per_id[rid][order]

            self.values_per_id[rid] = self.values_per_id[rid][order]

            self.ages_per_id[rid] = self.ages_per_id[rid][order]

            assert all(self.times_per_id[rid][i] <= self.times_per_id[rid][i + 1]
                       for i in range(len(self.times_per_id[rid]) - 1))

            # Replacing the times by t_i - t_0
            #self.times_per_id[rid] = self.times_per_id[rid] - self.times_per_id[rid][0]

            # Updating min and max times
            self.min_time = min(self.min_time, self.times_per_id[rid][0])
            self.max_time = max(self.max_time, self.times_per_id[rid][-1])

            # Converting to torch tensors.
            self.times_per_id[rid] = torch.from_numpy(np.array(self.times_per_id[rid])).unsqueeze(1).type(self.type)
            self.values_per_id[rid] = torch.from_numpy(np.array(self.values_per_id[rid], dtype=np.float64)).type(self.type)
            self.ages_per_id[rid] = torch.from_numpy(np.array(self.ages_per_id[rid])).type(self.type)

            # if len(self.times_per_id[rid]) <= 1:
            #     rids_to_remove.append(rid)

        # for rid in rids_to_remove:
        #     del self.times_per_id[rid]
        #     if self.load_on_the_fly:
        #         del self.paths_per_id[rid]
        #     else:
        #         del self.values_per_id[rid]
        #     del self.ages_per_id[rid]
        #     self.rids.remove(rid)

        print('Min time {} Max time {}'.format(self.min_time, self.max_time))

    def keep_only_n_observations_per_subject(self, n):
        print("Processing the dataset to only keep {} observations per subject".format(n))
        for rid in self.times_per_id.keys():
            self.times_per_id[rid] = self.times_per_id[rid][:min(len(self.times_per_id[rid]), n)]
            self.ages_per_id[rid] = self.ages_per_id[rid][:min(len(self.ages_per_id[rid]), n)]
            self.values_per_id[rid] = self.values_per_id[rid][:min(len(self.values_per_id[rid]), n)]

    def __len__(self):
        return len(self.ages_per_id)

    def __getitem__(self, i):
        rid = self.rids[i]
        return self.get_item_by_id(rid)

    def get_item_by_id(self, rid):
        return {
            'times': self.times_per_id[rid],
            'ages': self.ages_per_id[rid],
            'values': self.values_per_id[rid],
            'idx': rid
        }

    def save(self, filename):
        raise NotImplementedError('Should be implemented by child classes')

    def print_dataset_statistics(self):
        # Computing the average number of obs and the standard deviation
        obs_numbers = []
        for (rid, val) in self.times_per_id.items():
            obs_numbers.append(len(val))

        print('Number of visits {} (+/-) {}'.format(np.mean(obs_numbers), np.std(obs_numbers)))

    def total_number_of_observations(self):
        return len(self.ages)
