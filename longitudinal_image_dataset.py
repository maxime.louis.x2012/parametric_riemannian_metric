import numpy as np
from longitudinal_dataset import LongitudinalDataset
import pickle as pickle


class LongitudinalImageDataset(LongitudinalDataset):
    """Only handles batch size of 1..."""

    def __init__(self, ids, images_path, times, image_shape, image_dimension=2, use_cuda=False, image_255=True,
                 load_on_the_fly=False, ages_std=None, ages_mean=None):
        super(LongitudinalImageDataset, self).__init__(ids, times, use_cuda=use_cuda,
                                                       ages_std=ages_std, ages_mean=ages_mean)

        assert len(times) == len(images_path)

        self.images_path = images_path
        self.images_per_id = {}
        self.image_shape = image_shape
        self.image_dimension = image_dimension
        self.image_255 = image_255    # what kind of normalization is asked for
        assert len(self.image_shape) == image_dimension

        self.update()

    def _load_image(self, path):
        img = np.load(path).reshape((1,) + self.image_shape).astype(np.float32)
        if self.image_255:
            img = img / 255.
        return img

    def load_value(self, i):
        return self._load_image(self.images_path[i])

    def save(self, filename):
        pickle.dump((self.ids, self.images_path, self.ages, self.image_shape, self.image_dimension, self.image_255,
                     False, self.ages_std, self.ages_mean), open(filename, 'wb'))


def load_image_dataset(filename):
    ids, images_path, times, image_shape, image_dimension, image_255, load_on_the_fly, \
        ages_std, ages_mean = pickle.load(open(filename, 'rb'))

    # load_on_the_fly is legacy.

    return LongitudinalImageDataset(ids, images_path, times, image_shape, image_dimension,
                                    use_cuda=False, image_255=image_255, load_on_the_fly=load_on_the_fly,
                                    ages_std=ages_std, ages_mean=ages_mean)

