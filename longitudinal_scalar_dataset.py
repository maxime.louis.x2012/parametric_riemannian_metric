from longitudinal_dataset import LongitudinalDataset
import pickle as pickle
import torch


class LongitudinalScalarDataset(LongitudinalDataset):
    """Only handles batch size of 1..."""

    def __init__(self, ids, values, times, ages_std=None, ages_mean=None, type=torch.DoubleTensor):
        super(LongitudinalScalarDataset, self).__init__(ids, times, ages_std=ages_std, ages_mean=ages_mean, type=type)
        assert len(ids) == len(values)
        self.values = values

        self.update()

    def load_value(self, i):
        return self.values[i]

    def save(self, filename):
        pickle.dump((self.ids, self.values, self.ages, self.ages_std, self.ages_mean), open(filename, 'wb'))


def load_scalar_dataset(filename):
    ids, values, times, ages_std, ages_mean = pickle.load(open(filename, 'rb'))
    return LongitudinalScalarDataset(ids, values, times, ages_std=ages_std, ages_mean=ages_mean)