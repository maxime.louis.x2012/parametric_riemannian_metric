import os
import torch
import numpy as np
from exponential_factory import ExponentialFactory
from utils import create_regular_grid_of_points, plot_progression, plot_metric, distance_between_metrics, sample_random_interpolation_values
from torch.optim import Adam
from matplotlib.pyplot import cm
import argparse
import platform
if platform.system() == 'Linux':
    import matplotlib as matplotlib
    matplotlib.use('agg')
import matplotlib.pyplot as plt


def plot_trajectories(trajectories, linestyle='-', max_plots=100, ax=None):
    color = cm.rainbow(np.linspace(0, 1, min(max_plots, len(trajectories))))
    for i, traj in enumerate(trajectories):
        if ax is None:
            plt.plot(traj.detach().numpy()[:, 0], traj.detach().numpy()[:, 1], linestyle=linestyle, c=color[i])
        else:
            ax.plot(traj.detach().numpy()[:, 0], traj.detach().numpy()[:, 1], linestyle=linestyle, c=color[i])
        if i >= max_plots - 1:
            break


def compute_trajectories(initial_positions, initial_velocities, exponential):
    final_positions = []
    trajectories = []

    for i in range(len(initial_positions)):
        exponential.set_initial_position(initial_positions[i])
        exponential.set_initial_velocity(initial_velocities[i])
        exponential.update()
        final_positions.append(exponential.get_final_position())
        trajectories.append(exponential.get_trajectory())

    print('{} trajectories have been computed'.format(len(initial_positions)))
    return final_positions, trajectories


def sample_geodesics(n, exponential, tensor_type=torch.DoubleTensor):
    initial_positions = torch.from_numpy(np.random.uniform(size=(n, exponential.dimension))).type(tensor_type)
    initial_velocities = torch.from_numpy(np.random.normal(size=(n, exponential.dimension))).type(tensor_type) * 0.8

    final_positions, trajectories = compute_trajectories(initial_positions, initial_velocities, exponential)
    return initial_positions, initial_velocities, final_positions, trajectories


def sample_geodesics_uniform(width, n_directions, exponential, tensor_type=torch.DoubleTensor, velocity_norm=1.):
    if width < 0.9:
        # We sample a uniformly spaced grid in 0 1
        box = np.zeros((exponential.dimension, 2))
        box[:, 0] = np.zeros(exponential.dimension)
        box[:, 1] = np.ones(exponential.dimension)
        initial_positions_aux = torch.from_numpy(create_regular_grid_of_points(box, width)).type(tensor_type)
    else:
        initial_positions_aux = torch.from_numpy(np.array([[0.5, 0.5]])).type(tensor_type)

    # For each point, we compute regularly spaced
    initial_positions = []
    initial_velocities = []
    unit_vectors = [np.array([np.cos(x), np.sin(x)]) * 0.3 * velocity_norm for x in np.linspace(0., 2 * np.pi, n_directions)]
    for pos in initial_positions_aux:
        for vector in unit_vectors:
            initial_positions.append(pos)
            initial_velocities.append(vector)

    initial_velocities = torch.from_numpy(np.array(initial_velocities)).type(tensor_type)

    final_positions, trajectories = compute_trajectories(initial_positions, initial_velocities, exponential)
    return initial_positions, initial_velocities, final_positions, trajectories


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-n_dir", "-n_dir", default=10)
    parser.add_argument("-width", "-width", default=0.2)

    args = parser.parse_args()
    n_dir = int(args.n_dir)
    geodesic_sampling_width = float(args.width)

    width = 0.15

    data_dim = 2
    spd_dim = int(data_dim * (data_dim + 1) / 2)
    tensor_type = torch.DoubleTensor
    nb_time_points = 10
    n_steps = 1000

    ref_point = torch.from_numpy(np.array([0.5, 0.5]))

    output_dir = os.path.join('../output/output_width_{}_n_dir_{}'.format(geodesic_sampling_width, n_dir))

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    box = np.zeros((data_dim, 2))
    box[:, 0] = np.zeros(data_dim)
    box[:, 1] = np.ones(data_dim)
    interpolation_points = torch.from_numpy(create_regular_grid_of_points(box, width)).type(tensor_type)
    interpolation_values_target = torch.from_numpy(sample_random_interpolation_values(len(interpolation_points), data_dim)).type(tensor_type)
    interpolation_values_target = interpolation_values_target / torch.sum(interpolation_values_target)

    manifold_parameters_target = {'width': width,
                                   'interpolation_points' : interpolation_points,
                                   'interpolation_values' : interpolation_values_target
                                    }

    exponential_factory = ExponentialFactory('parametric', data_dim, manifold_parameters_target)

    exponential_target = exponential_factory.create()
    exponential_target.number_of_time_points = nb_time_points
    exponential_target.update_scaling_factor(ref_point, data_dim + 1)

    plt.clf()
    plot_metric(exponential_target)
    plt.savefig(os.path.join(output_dir, 'target_metric.pdf'))
    plt.close()

    print('Number of interpolation points {}'.format(len(interpolation_points)))

    np.save(os.path.join(output_dir, 'target_tensor.npy'), interpolation_values_target.detach().numpy())

    interpolation_values_learned = torch.from_numpy(sample_random_interpolation_values(len(interpolation_points), data_dim))\
        .type(tensor_type)
    interpolation_values_learned.requires_grad_(True)

    manifold_parameters_learned = {'width': width,
                           'interpolation_points' : interpolation_points,
                           'interpolation_values' : interpolation_values_learned
                           }

    exponential_factory_learned = ExponentialFactory('parametric', data_dim, manifold_parameters_learned)
    exponential_learned = exponential_factory_learned.create()
    exponential_learned.number_of_time_points = nb_time_points

    optimizer = Adam(list([interpolation_values_learned]), lr=1e-2)

    geodesic_losses = []
    tensor_losses = []

    for step in range(n_steps):

        if step == 0:
            #initial_positions, initial_velocities, final_positions, trajectories = sample_geodesics(20, exponential_target)
            initial_positions, initial_velocities, final_positions, trajectories = \
                sample_geodesics_uniform(geodesic_sampling_width, n_dir, exponential_target)

            plt.clf()
            plot_trajectories(trajectories, max_plots=10000)
            plt.savefig(os.path.join(output_dir, 'trajectories_target.pdf'), bbox_inches='tight', pad_inches=0)
            plt.close()

        optimizer.zero_grad()

        loss = 0.

        exponential_learned.set_parameters(interpolation_values_learned)

        trajectories_learned = []

        for i in range(len(initial_positions)):
            exponential_learned.set_initial_position(initial_positions[i])
            exponential_learned.set_initial_velocity(initial_velocities[i])
            exponential_learned.update()

            trajectories_learned.append(exponential_learned.get_trajectory())

            loss += torch.sum((trajectories_learned[-1] - trajectories[i])**2)

        loss = loss / len(initial_positions) # rescaling (necessary for proper lr)

        loss.backward(retain_graph=True)

        if step % 10 == 0:
            plt.clf()
            plot_trajectories(trajectories, linestyle='-', max_plots=10000)
            plot_trajectories(trajectories_learned, linestyle='--', max_plots=10000)
            plt.savefig(os.path.join(output_dir, 'trajectories_learned.pdf'))
            plt.close()

        optimizer.step()
        interpolation_values_learned = exponential_learned.project_metric_parameters(interpolation_values_learned)
        exponential_learned.set_parameters(interpolation_values_learned)

        exponential_learned.update_scaling_factor(ref_point, data_dim + 1)

        tensor_error = distance_between_metrics(exponential_learned, exponential_target)
        print('Step {}/{} Loss {} tensor error {}'.format(step, n_steps, loss.detach().numpy(), tensor_error))

        plt.clf()
        ax = plot_metric(exponential_target, style='other')
        plot_metric(exponential_learned, ax=ax)
        if step == 0:
            plt.savefig(os.path.join(output_dir, 'learned_metric_initial.pdf'), bbox_inches='tight', pad_inches=0)
            plt.close()
        else:
            plt.savefig(os.path.join(output_dir, 'learned_metric.pdf'), bbox_inches='tight', pad_inches=0)
            plt.close()

        exponential_learned.scaling_factor = 1.
        geodesic_losses.append(loss.detach().numpy())
        tensor_losses.append(tensor_error)

        plt.clf()
        plot_progression([geodesic_losses, tensor_losses], ['geodesic loss', 'tensor error'], output_dir, 'losses.pdf')
        plt.close()

        geodesic_losses_save = np.array(geodesic_losses)
        np.savetxt(os.path.join(output_dir, 'geodesic_losses.txt'), geodesic_losses_save)

        tensor_losses_save = np.array(tensor_losses)
        np.savetxt(os.path.join(output_dir, 'tensor_losses.txt'), tensor_losses_save)

        np.save(os.path.join(output_dir, 'learned_tensor.npy'), interpolation_values_learned.detach().numpy())






















