import numpy as np
import torch
from utils import uncholeskify

from exponential_interface import ExponentialInterface


"""
Class with a parametric inverse metric: $$g_{\theta}(q) = \sum_{i=1}^n \alpha_i \exp{-\frac {\|x-q\|^2} {2 \sigma^2}$$ 
The metric_parameters for this class is the set of symmetric positive definite matrices from which we interpolate. 
It is a (nb_points, dimension*(dimension+1)/2) tensor, enumerated by lines.
"""

class ParametricExponential(ExponentialInterface):

    def __init__(self, dimension):
        ExponentialInterface.__init__(self, dimension)
        self.dimension = dimension
        self.spd_dimension = int(self.dimension * (self.dimension + 1) / 2)
        self.number_of_interpolation_points = None
        self.width = None
        # List of points in the space, from which the metric is interpolated
        self.interpolation_points_torch = None
        # Tensor, shape (number of points, dimension, dimension)
        self.interpolation_values_torch = None

        self.diagonal_indices = None

        self.scaling_factor = 1.

        self.has_closed_form = False
        self.has_closed_form_dp = True
        self.has_closed_form_parallel_transport = False

    def inverse_metric(self, q):
        squared_distances = torch.sum(((self.interpolation_points_torch - q)**2.).view(self.number_of_interpolation_points, self.dimension), 1)

        return self.scaling_factor * torch.sum(self.interpolation_values_torch
                         * torch.exp(-1.*squared_distances/self.width**2)
                         .view(self.number_of_interpolation_points, 1, 1)
                         .expand(-1, -1, self.dimension), 0) + torch.eye(self.dimension).type(self.interpolation_values_torch.type())

    def dp(self, q, p):
        differences = (q - self.interpolation_points_torch).view(self.number_of_interpolation_points, self.dimension)
        psp = torch.bmm(p.view(1, 1, self.dimension).expand(self.number_of_interpolation_points, 1, self.dimension),
                         torch.bmm(self.interpolation_values_torch,
                    p.view(1, self.dimension, 1).expand(self.number_of_interpolation_points, self.dimension, 1)))\
            .view(self.number_of_interpolation_points, 1)\
            .expand(self.number_of_interpolation_points, self.dimension)

        norm_gradient = differences
        weight = torch.exp(- torch.sum(differences**2, 1)/self.width**2).view(self.number_of_interpolation_points, -1).expand(self.number_of_interpolation_points, self.dimension)
        return -self.scaling_factor/self.width**2 * torch.sum(psp * norm_gradient * weight, 0)

    def set_parameters(self, extra_parameters):
        """
        In this case, the parameters are the interpolation values
        """

        dim = self.dimension
        size = extra_parameters.size()
        assert size[0] == self.interpolation_points_torch.size()[0]
        assert size[1] == self.spd_dimension
        symmetric_matrices = uncholeskify(extra_parameters, dim)

        self.interpolation_values_torch = symmetric_matrices
        self.number_of_interpolation_points = extra_parameters.size()[0]
        self.is_modified = True

    def _get_diagonal_indices(self):
        if self.diagonal_indices is None:
            diagonal_indices = []
            spacing = 0
            pos_in_line = 0
            dim = self.dimension
            for j in range(int(dim*(dim+1)/2) - 1, -1, -1):
                if pos_in_line == spacing:
                    spacing += 1
                    pos_in_line = 0
                    diagonal_indices.append(j)
                else:
                    pos_in_line += 1
            self.diagonal_indices = np.array(diagonal_indices)

        return self.diagonal_indices

    def update_scaling_factor(self, point, target_value):
        """
        Updates the metric scaling factor so as to maintain a trace of $g^{-1}$ of target_value at point
        """
        self.scaling_factor = 1.
        trace = np.trace(self.inverse_metric(point).detach().numpy()) - self.dimension
        self.scaling_factor = (target_value - self.dimension) / trace

    def project_metric_parameters(self, metric_parameters):
        """
        :param metric_parameters: numpy array of shape (number of points, dimension*(dimension+1)/2)
        we must check positivity of the diagonal coefficients of the lower matrices.
        :return:
        """
        diagonal_indices = self._get_diagonal_indices()

        # Positivity for each diagonal coefficient.
        for i in range(len(metric_parameters)):
            for j in diagonal_indices:
                if metric_parameters.data[i][j] < 0:
                    metric_parameters.data[i][j] = 0

        return metric_parameters

    # def project_metric_parameters_gradient(self, metric_parameters, metric_parameters_gradient):
    #     """
    #     Projection to ensure identifiability of the geodesic parametrizations.
    #     """
    #     diagonal_indices = self._get_diagonal_indices()
    #
    #     out = metric_parameters_gradient
    #
    #     for j in diagonal_indices:
    #         orthogonal_gradient = metric_parameters[:, j]
    #         orthogonal_gradient /= np.linalg.norm(orthogonal_gradient)
    #         out[:, j] -= np.dot(out[:, j], orthogonal_gradient)
    #
    #     return out

    # @staticmethod
    # def choleskify(l):
    #     print("Choleskify needs to be checked !")
    #     return l[:, torch.triu(torch.ones(3, 3)) == 1]


if __name__ == '__main__':

    from exponential_factory import ExponentialFactory
    import os
    import torch
    import numpy as np
    from generic_geodesic import GenericGeodesic
    import matplotlib.pyplot as plt
    import matplotlib.cm as mplcm
    import matplotlib.colors as colors

    tensor_type = torch.DoubleTensor
    from matplotlib.pyplot import cm
    from utils import create_regular_grid_of_points, get_metric_ellipses_info
    from metric_from_geodesics import sample_geodesics, compute_trajectories, plot_trajectories, \
        sample_geodesics_uniform

    tensor_type = torch.DoubleTensor

    manifold_type = 'parametric'
    dimension = 1
    spd_dimension = int(dimension * (dimension + 1) / 2)
    number_of_interpolation_points = 2
    width = 1. / number_of_interpolation_points

    interpolation_points_np = np.linspace(0., 1., number_of_interpolation_points).reshape(
        number_of_interpolation_points, dimension)

    interpolation_points = torch.from_numpy(interpolation_points_np).type(tensor_type)
    interpolation_values = np.ones((number_of_interpolation_points, spd_dimension))
    interpolation_values = torch.from_numpy(interpolation_values).type(tensor_type)
    #torch.from_numpy(
        # np.random.uniform(size=(number_of_interpolation_points, spd_dimension))).type(tensor_type)
    print(interpolation_values)

    manifold_parameters = {'width': width,
                           'interpolation_points': interpolation_points,
                           'interpolation_values': interpolation_values}

    factory = ExponentialFactory(manifold_type=manifold_type, manifold_parameters=manifold_parameters,
                                 dimension=dimension)
    geodesic = GenericGeodesic(factory)
    geodesic.set_t0(0.)
    geodesic.set_tmin(-0.5)
    geodesic.set_tmax(1.5)
    geodesic.concentration_of_time_points = 500

    p0 = np.zeros(1)

    p0 = torch.from_numpy(p0).type(tensor_type)
    v0 = torch.from_numpy(np.ones(1)).type(tensor_type) * 3.
    geodesic.set_position_t0(p0)
    geodesic.set_velocity_t0(v0)

    f, (ax1, ax2) = plt.subplots(1, 2, figsize=(16, 8))

    for i in range(1):
        #interpolation_values = torch.from_numpy(
#            np.random.uniform(size=(number_of_interpolation_points, spd_dimension))).type(tensor_type)
        #interpolation_values = interpolation_values / torch.sum(1. / interpolation_values)
        #geodesic.set_parameters(interpolation_values)
        geodesic.set_position_t0(p0)
        geodesic.set_velocity_t0(v0)
        geodesic.update()
        traj = np.array([elt.data.numpy() for elt in geodesic.get_geodesic_trajectory()])
        times = geodesic.get_times()
        # We also get the values of the inverse metric at the points where the geodesic is
        inverse_metric_values = [geodesic.forward_exponential.inverse_metric(elt) for elt in
                                 geodesic.get_geodesic_trajectory()]
        inverse_metric_values = np.array(inverse_metric_values)
        print(np.min(inverse_metric_values), np.max(inverse_metric_values))
        for d in range(len(traj[0])):
            # plt.scatter(times, traj[:,d], c=inverse_metric_values)
            ax1.plot(times, traj[:, d])
        ax2.plot(traj, inverse_metric_values)
    ax1.set_xlabel('Time', fontsize=25)
    ax1.set_ylabel('Coordinate', fontsize=25)
    ax1.set_title('Geodesics', fontsize=25)
    ax2.set_xlabel('Coordinate', fontsize=25)
    ax2.set_ylabel('Inverse metric', fontsize=25)
    ax2.set_title('Inverse metric', fontsize=25)

    #plt.savefig('../Figures/sample_parametric_geodesics.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()

    # plt.plot(interpolation_points_np, interpolation_values.detach().numpy())
    # plt.show()