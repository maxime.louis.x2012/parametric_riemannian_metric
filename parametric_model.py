import numpy as np
import os
import torch
from torch.utils.data import DataLoader
from networks import ScalarRNN
import matplotlib.pyplot as plt
from utils import create_regular_grid_of_points, initialize_metric_tensor_values
from torch.distributions.normal import Normal
from torch.optim import Adam
from generic_spatiotemporal_reference_frame import GenericSpatiotemporalReferenceFrame
from exponential_factory import ExponentialFactory


class ParametricModel:

    def __init__(self, data_dim, width, encoder_hidden_dim=16, nb_sources=2, labels=None,
                 type=torch.DoubleTensor):
        self.data_dim = data_dim
        self.spd_dim = int(data_dim * (data_dim + 1) / 2)
        self.nb_sources = nb_sources
        self.type = type
        self.width = width

        self.modulation_matrix = torch.from_numpy(np.random.normal(size=(data_dim, nb_sources)))\
            .type(self.type).requires_grad_(True)
        self.manifold_type = 'parametric'

        self.labels = labels

        self.metric_parameters, self.interpolation_points = self.initialize_metric_parameters()

        manifold_parameters = {
            'width': self.width,
            'interpolation_points': self.interpolation_points,
            'interpolation_values': self.metric_parameters
        }

        # factory = ExponentialFactory(manifold_type=self.manifold_type,
        #                              manifold_parameters=manifold_parameters,
        #                              dimension=data_dim)
        #
        factory = ExponentialFactory(manifold_type='euclidean',
                                     dimension=data_dim)

        self.encoder = ScalarRNN(data_dim+1, encoder_hidden_dim, out_dim=2 + nb_sources)
        if type == torch.FloatTensor:
            self.encoder.float()
        else:
            self.encoder.double()

        self.reference_frame = GenericSpatiotemporalReferenceFrame(factory, type=self.type)
        self.reference_frame.set_concentration_of_time_points(10)
        self.reference_frame.set_number_of_time_points(20)

        self.xi_mean = torch.zeros(1).type(self.type)
        self.tau_mean = torch.zeros(1).type(self.type)
        self.xi_std = torch.ones(1).type(self.type) * 0.3
        self.tau_std = torch.ones(1).type(self.type) * 10.

        self.freeze_taus = False
        self.freeze_alphas = False
        self.freeze_v0 = False
        self.freeze_p0 = False
        self.freeze_t0 = False

        self.t0 = (torch.ones(1) * 72.).type(self.type).requires_grad_(not self.freeze_t0)
        self.p0 = torch.from_numpy(np.array([0.3, 0.1, 0.3, 0.1])).type(self.type).requires_grad_(not self.freeze_p0)
        self.v0 = torch.from_numpy(np.array([0.05, 0.05, 0.04, 0.04])).type(self.type).requires_grad_(not self.freeze_v0)

    def initialize_metric_parameters(self):
        box = np.zeros((self.data_dim, 2))
        box[:, 0] = np.zeros(self.data_dim)
        box[:, 1] = np.ones(self.data_dim)
        interpolation_points = torch.from_numpy(create_regular_grid_of_points(box, self.width))\
            .type(self.type)
        interpolation_values = torch.from_numpy(
            initialize_metric_tensor_values(len(interpolation_points), self.data_dim)
        ).type(self.type).requires_grad_(True)

        print('Initialized {} interpolation points'.format(len(interpolation_points)))

        return interpolation_values, interpolation_points

    def encode(self, times, values):
        out = self.encoder.get_sequence_output(times, values)  # xi, tau and w
        return out

    def get_individual_info(self, times, sample):
        if self.freeze_taus:
            tau = torch.from_numpy(np.zeros(1)).type(self.type)
        else:
            tau = sample[1]
        if self.freeze_alphas:
            xi = torch.from_numpy(np.zeros(1)).type(self.type)
        else:
            xi = sample[0]
        sources = sample[2:]
        geodesic_times = torch.exp(xi) * (times - tau - self.t0) + self.t0
        return xi, tau, sources, geodesic_times

    def decode(self, times, sources):
        assert len(times) == len(sources), 'Should not happen, handled in estimator'
        tmin = np.min(times.detach().numpy())
        tmax = np.max(times.detach().numpy())

        # The expensive operation
        self.update_reference_frame(tmin, tmax)

        reconstructions = torch.from_numpy(np.zeros((len(times), self.data_dim))).type(self.type)
        for i, (t, source) in enumerate(zip(times, sources)):
            reconstructions[i] = self.reference_frame.get_position(t, source)

        return reconstructions

    def parameters(self, mode=None):
        out = []
        if mode is None or mode == 'pop':
            out = out + [self.modulation_matrix]
            if not self.freeze_p0:
                out = out + [self.p0]
            if not self.freeze_v0:
                out = out + [self.v0]
            if not self.freeze_t0:
                out = out + [self.t0]
        if mode is None or mode == 'encoder':
            out = out + list(self.encoder.parameters())
        #out = out + [self.metric_parameters]
        return out

    def update_reference_frame(self, tmin, tmax):
        tmin = min(float(self.t0), tmin)
        tmax = max(float(self.t0), tmax)
        self.reference_frame.set_t0(float(self.t0))
        self.reference_frame.set_tmin(tmin)
        self.reference_frame.set_tmax(tmax)
        self.reference_frame.set_position_t0(self.p0)
        self.reference_frame.set_velocity_t0(self.v0)
        self.reference_frame.set_modulation_matrix_t0(self.modulation_matrix)
        self.reference_frame.set_metric_parameters(self.metric_parameters)
        self.reference_frame.update()

    def compute_regularity(self, xis, taus, sources):
        out = 0.
        # We sum the three regularity terms
        xi_mean = self.get_xi_mean()
        xi_std = self.get_xi_std()
        xi_distribution = Normal(loc=xi_mean, scale=xi_std)

        out = out - torch.sum(xi_distribution.log_prob(xis))

        tau_mean = self.get_tau_mean()
        tau_std = self.get_tau_std()
        tau_distribution = Normal(loc=tau_mean, scale=tau_std)

        out = out - torch.sum(tau_distribution.log_prob(taus))

        sources_mean = torch.zeros(self.nb_sources).type(self.type)
        sources_std = torch.ones(self.nb_sources).type(self.type)
        sources_distribution = Normal(loc=sources_mean, scale=sources_std)

        out = out - torch.sum(sources_distribution.log_prob(sources))

        return out

    def get_xi_mean(self):
        return self.xi_mean

    def get_tau_mean(self):
        return self.tau_mean

    def get_xi_std(self):
        return self.xi_std

    def get_tau_std(self):
        return self.tau_std

    def plot_average_trajectory(self, dir):
        # We get the values of the average trajectory !
        colors = ['b', 'g', 'r', 'c']
        times = self.reference_frame.geodesic.get_times()
        trajectory = self.reference_frame.geodesic.get_geodesic_trajectory()
        trajectory = [elt.cpu().detach().numpy() for elt in trajectory]
        trajectory = np.array(trajectory)
        plt.clf()
        plt.figure(figsize=(10, 10))
        for i in range(len(trajectory[0])):
            plt.plot(times, trajectory[:, i], label=self.labels[i], c=colors[i])
        plt.legend()
        plt.savefig(os.path.join(dir, 'mean_trajectory.pdf'))

    def encode_all(self, dataloader):
        ids = []
        sources, xis, taus = [], [], []
        for data in dataloader:
            ids.append(data['idx'])
            times = data['times'].squeeze(0)
            values = data['values'].squeeze(0)
            indiv_var = self.encode(times, values)
            xi, tau, source, _ = self.get_individual_info(times, indiv_var)
            sources.append(source.cpu().detach().numpy())
            taus.append(tau.cpu().detach().numpy())
            xis.append(xi.cpu().detach().numpy())
        return ids, sources, xis, taus

    def save(self, output_dir):
        # minor saving for now
        t0, v0, p0 = self.t0.detach().numpy(), self.v0.detach().numpy(), self.p0.detach().numpy()
        metric_parameters_np = self.metric_parameters.detach().numpy()
        dico = {'t0': t0,
                'p0': p0,
                'v0': v0,
                'metric_parameters' : metric_parameters_np,
                'modulation_matrix' : self.modulation_matrix.detach().numpy(),
                'interpolation_points': self.interpolation_points.detach().numpy()
                }
        np.save(os.path.join(output_dir, 'global_params.npy'), dico)
        np.savetxt(os.path.join(output_dir, 'metric_parameters.txt'), self.metric_parameters.detach().numpy())

    def project_metric_parameters(self):
        self.metric_parameters.data = self.reference_frame.exponential.project_metric_parameters(self.metric_parameters.data)
        # TODO: add some identifiability here.

    def linear_initialization(self, dataset):
        dataloader = DataLoader(dataset, batch_size=1, shuffle=False)
        slopes, intercepts, ages, obs, bl_ages = self._initial_linear_regressions(dataset)
        mean_slope = np.mean(slopes, axis=0).flatten()
        mean_intercept = np.mean(intercepts, axis=0).flatten()
        t0 = np.mean(ages)
        p0 = np.mean(obs, axis=0)
        v0 = mean_slope
        alphas, taus = self._initial_alphas_taus(slopes, intercepts, mean_slope, mean_intercept, t0, bl_ages)
        sources, modulation_matrix = self._initial_sources_and_modulation_matrix(dataloader, v0, p0)

        alphas_torch = torch.from_numpy(alphas).type(self.type)
        taus_torch = torch.from_numpy(taus).type(self.type)
        sources_torch = torch.from_numpy(sources).type(self.type)
        #
        #alphas_torch = torch.exp(torch.from_numpy(np.random.normal(0., 0.05, size=alphas_torch.numpy().shape)))
        #taus_torch = torch.from_numpy(np.random.normal(0., 0.5, size=taus_torch.numpy().shape))
        #sources_torch = torch.from_numpy(np.random.normal(0., 0.05, size=sources_torch.numpy().shape))

        self._initialize_encoder(dataloader, alphas_torch, taus_torch, sources_torch)

        # Now setting all the attributes !
        self.t0 = (torch.ones(1) * t0).type(self.type)
        self.v0 = torch.from_numpy(v0).type(self.type).requires_grad_(True)
        self.p0 = torch.from_numpy(p0).type(self.type).requires_grad_(True)
        self.modulation_matrix = torch.from_numpy(modulation_matrix).type(self.type).requires_grad_(True)

    def _initial_linear_regressions(self, dataset):
        from sklearn.linear_model import LinearRegression
        slopes, intercepts = [], []
        all_ages, all_obs = [], []
        baseline_ages = []
        for data in dataset:
            lr = LinearRegression()
            ages = data['ages'].squeeze(0).detach().numpy().reshape(-1, 1)
            values = data['values'].squeeze(0).detach().numpy()
            lr.fit(ages, values)
            slopes.append(lr.coef_)  # (data_dim, 1)
            intercepts.append(lr.intercept_)
            for ag in ages.flatten():
                all_ages.append(ag)
            baseline_ages.append(ages.flatten()[0])
            for val in values:
                all_obs.append(val)
        return np.array(slopes), np.array(intercepts), np.array(all_ages), np.array(all_obs), baseline_ages

    def _initial_alphas_taus(self, slopes, intercepts, mean_slope, mean_intercept, t0, bl_ages):
        alphas, taus = [], []
        for i, (slope, intercept) in enumerate(zip(slopes, intercepts)):
            alpha = np.linalg.norm(slope) / np.linalg.norm(mean_slope)
            alpha = max(0.3, min(alpha, 3.))
            # tau = np.linalg.norm(mean_intercept + mean_slope * t0 - intercept - mean_slope * alpha * t0) / np.linalg.norm(alpha * mean_slope)
            # tau = max(-1. * 15, min(tau, 15))  # some thresholding
            tau = t0 - bl_ages[i] # why this ? phi_i(t) = torch.exp(xi) * (times - tau - self.t0) + self.t0
            alphas.append(alpha)
            taus.append(tau)
        return np.array(alphas), np.array(taus)

    def _initial_sources_and_modulation_matrix(self, dataloader, v0, p0):
            unit_v0 = v0 / np.linalg.norm(v0)

            from sklearn.decomposition import PCA
            pca = PCA(n_components=self.nb_sources)

            mean_vectors = []
            for data in dataloader:
                values = data['values'].squeeze(0).cpu().detach().numpy()
                projected_vectors = []
                for val in values:
                    projected_vectors.append(val - p0 - np.dot(val, unit_v0) * v0)
                projected_vectors = np.array(projected_vectors)
                mean_vectors.append(np.mean(projected_vectors, 0))
            mean_vectors = np.array(mean_vectors)
            sources = pca.fit_transform(mean_vectors)
            modulation_matrix = np.transpose(pca.components_)

            # Some rescaling:
            for i in range(self.nb_sources):
                std = np.std(sources[:, i])
                sources[:, i] = (sources[:, i] - np.mean(sources[:, i])) / std
                modulation_matrix[:, i] *= std

            # Code has been checked, error is still large because of the wrong centering: we ignore

            return sources, modulation_matrix

    def _initialize_encoder(self, dataloader, alphas, taus, sources):
        n_epochs = 80
        optimizer = Adam(self.encoder.parameters(), lr=1e-3)
        for epoch in range(n_epochs):
            epoch_loss = 0.

            for i, data in enumerate(dataloader):
                optimizer.zero_grad()
                times = data['times'].squeeze(0)
                values = data['values'].squeeze(0)

                indiv_var = self.encode(times, values)

                loss = (indiv_var[0] - torch.log(alphas[i])) **2 + (indiv_var[1] - taus[i]) ** 2 \
                       + torch.sum((indiv_var[2:] - sources[i]) ** 2)

                loss.backward()
                optimizer.step()

                epoch_loss += float(loss)

            if epoch % 5 == 0:
                print('Epoch {}/{} loss {}'.format(epoch, n_epochs, epoch_loss))

    def plot_reconstructions(self, dataloader, output_dir):
        f, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(12, 12))

        for i, ax in enumerate(axes.flatten()):
            data = next(iter(dataloader))

            times = data['times'].squeeze(0)
            ages = data['ages'].squeeze(0)
            values = data['values'].squeeze(0)

            indiv_var = self.encode(times, values)
            xi, tau, sources, geodesic_times = self.get_individual_info(ages, indiv_var)
            reconstructed = self.decode(geodesic_times, sources.view(1, -1).expand(len(times), -1))

            colors = ['b', 'g', 'r', 'c']
            labels = ['memory', 'language', 'praxis', 'concentration']

            for l in range(self.data_dim):
                ax.plot(times.cpu().detach().numpy(),
                        reconstructed[:, l].cpu().detach().numpy(),
                        c=colors[l], label=labels[l], linestyle='dotted')
                ax.plot(times.cpu().detach().numpy(),
                        values[:, l].cpu().detach().numpy(),
                        c=colors[l])
                ax.legend()

        plt.legend()
        plt.savefig(os.path.join(output_dir, 'reconstructions.pdf'))

