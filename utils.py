import torch
import platform
if platform.system() == 'Linux':
    import matplotlib as matplotlib
    matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import math
from scipy.linalg import sqrtm
import numpy as np
from numpy.linalg import inv

def uncholeskify(l, dim):
    """
    Takes a tensor l of shape (n_cp, dimension*(dimension+1)/2)
    and returns out a tensor of shape (n_cp, dimension, dimension)
    such that out[i] = Upper(l[i]).transpose() * Upper(l[i])
    """
    out = torch.from_numpy(np.zeros((l.size()[0], dim, dim))).type(l.type())
    ones = torch.ones(dim, dim).type(l.type())
    for i in range(l.size()[0]):
        aux = torch.from_numpy(np.zeros((dim, dim))).type(l.type())
        aux[torch.triu(ones) == 1] = l[i]
        out[i] = aux
    return torch.bmm(torch.transpose(out, 1, 2), out)


def plot_progression(l, names, output_dir, output_name):
    plt.clf()
    fig, axes = plt.subplots(len(l), 1, figsize=(9, 3 * len(l)), sharex=True)
    for elt, name, ax in zip(l, names, axes):
        ax.plot(range(len(elt)), elt, label=name)
        ax.set_title(name)
    plt.savefig(os.path.join(output_dir, output_name))

def create_regular_grid_of_points(box, spacing):
    """
    Creates a regular grid of 2D or 3D points, as a numpy array of size nb_of_points x dimension.
    box: (dimension, 2)
    """
    dimension = len(box)

    axis = []
    for d in range(dimension):
        min = box[d, 0]
        max = box[d, 1]
        length = max - min
        assert (length > 0)

        offset = 0.5 * (length - spacing * math.floor(length / spacing))
        axis.append(np.arange(min + offset, max + 1e-10, spacing))

    if dimension == 1:
        control_points = np.zeros((len(axis[0]), dimension))
        control_points[:, 0] = axis[0].flatten()

    elif dimension == 2:
        x_axis, y_axis = np.meshgrid(axis[0], axis[1])

        assert (x_axis.shape == y_axis.shape)
        number_of_control_points = x_axis.flatten().shape[0]
        control_points = np.zeros((number_of_control_points, dimension))

        control_points[:, 0] = x_axis.flatten()
        control_points[:, 1] = y_axis.flatten()

    elif dimension == 3:
        x_axis, y_axis, z_axis = np.meshgrid(axis[0], axis[1], axis[2])

        assert (x_axis.shape == y_axis.shape)
        assert (x_axis.shape == z_axis.shape)
        number_of_control_points = x_axis.flatten().shape[0]
        control_points = np.zeros((number_of_control_points, dimension))

        control_points[:, 0] = x_axis.flatten()
        control_points[:, 1] = y_axis.flatten()
        control_points[:, 2] = z_axis.flatten()

    elif dimension == 4:
        x_axis, y_axis, z_axis, t_axis = np.meshgrid(axis[0], axis[1], axis[2], axis[3])

        assert (x_axis.shape == y_axis.shape)
        assert (x_axis.shape == z_axis.shape)
        number_of_control_points = x_axis.flatten().shape[0]
        control_points = np.zeros((number_of_control_points, dimension))

        control_points[:, 0] = x_axis.flatten()
        control_points[:, 1] = y_axis.flatten()
        control_points[:, 2] = z_axis.flatten()
        control_points[:, 3] = t_axis.flatten()

    else:
        raise RuntimeError('Invalid ambient space dimension.')

    return control_points


def initialize_metric_tensor_values(nb, dim):
    diagonal_indices = []
    spacing = 0
    pos_in_line = 0
    for j in range(int(dim * (dim + 1) / 2) - 1, -1, -1):
        if pos_in_line == spacing:
            spacing += 1
            pos_in_line = 0
            diagonal_indices.append(j)
        else:
            pos_in_line += 1

    metric_parameters = np.zeros((nb, int(dim * (dim + 1) / 2)))
    val = np.sqrt(1. / nb)
    for i in range(len(metric_parameters)):
        for k in range(len(metric_parameters[i])):
            if k in diagonal_indices:
                metric_parameters[i, k] = val

    return metric_parameters


def get_metric_ellipses_info(exponential):
    e1 = np.zeros(2)
    e2 = np.zeros(2)
    e1[0] = 1
    e2[1] = 1
    box = np.zeros((2, 2))
    box[:, 0] = np.zeros(2)
    box[:, 1] = np.ones(2)
    positions_for_plot = torch.from_numpy(create_regular_grid_of_points(box, 0.1)).type(exponential.interpolation_points_torch.type())
    positions, heights, widths, angles = [], [], [], []
    for pos in positions_for_plot:
        matrix = np.linalg.inv(exponential.inverse_metric(pos).detach().numpy())
        position = pos.detach().numpy()
        eigenvalues, eigenvectors = np.linalg.eigh(matrix)
        angle = np.arccos(np.dot(eigenvectors[:, 0], e1)) * 360 / (2 * np.pi)
        widths.append(eigenvalues[1])
        heights.append(eigenvalues[0])
        angles.append(angle)
        positions.append(position)
    max_length = max(np.max(widths), np.max(heights)) * 10.  # to have a good visual
    widths = widths / max_length
    heights = heights / max_length
    return positions, heights, widths, angles


def plot_metric(exponential, style='reference', ax=None):
    from matplotlib.pyplot import cm
    ells = []
    positions, heights, widths, angles = get_metric_ellipses_info(exponential)
    from matplotlib.patches import Ellipse
    for (position, width, height, angle) in zip(positions, heights, widths, angles):
        if style == 'reference':
            ellipse = Ellipse(xy=(position[0], position[1]), width=width, height=height, angle=angle)
        else:
            ellipse = Ellipse(xy=(position[0], position[1]), width=width, height=height, angle=angle, linewidth=4, fill=False)
        ells.append(ellipse)
    if ax == None:
        fig, ax = plt.subplots(subplot_kw={'aspect': 'equal'}, figsize=(30, 30))
    plt.axis('off')
    colors = cm.rainbow(np.linspace(0, 1, len(ells)*10))
    for i, e in enumerate(ells):
        ax.add_artist(e)
        e.set_clip_box(ax.bbox)
        e.set_alpha(0.5)
        e.set_facecolor(colors[i])

    plt.xlim(-0.2, 1.2)
    plt.ylim(-0.2, 1.2)
    return ax


def affine_invariant_distance(m1, m2):
    sqrt = sqrtm(m1)
    inverse_sqrt = inv(sqrt)
    mat = np.matmul(np.matmul(inverse_sqrt, m2), inverse_sqrt)  
    eigenvalues, _ = np.linalg.eigh(mat)
    return np.sum(np.log(eigenvalues)**2)

def distance_between_metrics(exp1, exp2):
    box = np.zeros((2, 2))
    box[:, 0] = np.zeros(2)
    box[:, 1] = np.ones(2)
    positions_for_integration = torch.from_numpy(create_regular_grid_of_points(box, 0.1)).type(
        exp1.interpolation_points_torch.type())
    dist = 0.
    for pos in positions_for_integration:
        m1 = np.linalg.inv(exp1.inverse_metric(pos).detach().numpy())
        m2 = np.linalg.inv(exp2.inverse_metric(pos).detach().numpy())
        dist += affine_invariant_distance(m1, m2)
    return dist / len(positions_for_integration)

def sample_random_interpolation_values(n, dim):
    diagonal_indices = []
    spacing = 0
    pos_in_line = 0
    for j in range(int(dim * (dim + 1) / 2) - 1, -1, -1):
        if pos_in_line == spacing:
            spacing += 1
            pos_in_line = 0
            diagonal_indices.append(j)
        else:
            pos_in_line += 1

    out = np.zeros((n, int(dim * (dim + 1) / 2)))
    for i in range(len(out)):
        for k in range(len(out[i])):
            if k in diagonal_indices:
                out[i, k] = np.random.uniform(0., 1.)
            else:
                out[i, k] = np.random.normal()
    return out
